# DEV-LOG
Just my private take on the great book `Essentials of Compilation: An Incremental Approach` and course (material) from:
* [Essentials of Compilation](https://github.com/IUCompilerCourse/Essentials-of-Compilation)
* [public student support code](https://github.com/IUCompilerCourse/public-student-support-code)
* [Lectures](https://iucompilercourse.github.io/IU-P423-P523-E313-E513-Fall-2020/)

Moved the original README.md from the student support code to ORIGINAL-README.md.

## Chapter 2.4 - Uniquify Variables 
Just implemented uniquify from excercise 1, which was pretty simple. We just use a symbol table to map from the original variable name to new unique variable names. So that each newly introduced (let-)variable will also extend the symbol table with a new generated variable (used `gensym`). But I did have a bug in it on the first try, that i didn't catch while testing some sample programms in excercise 2. For Example 
```
(uniquify (parse-program  '(program ()
   (let ([a 1])
      (let ([a a])
         (+ a (let ([a 4])
            a)))))))
```
The catch was that the expression for the new let-variable should use the original symbol table aka. the surrounding enviroment. I used the extend symbol-table, that of course lead to some bugs. Lucky for me that the run-tests catched by the example above. It couldn't handle the `(let ([a a]) ...)` with the uniquify translated to `(let ([a1234 a1234]) ...)`. aka a undefined variable ;)

## Chapter 2.5 - Remove Complex Operations
I don't know why exactly but at first i didn't really get the simple concept of `replace each complex operator with a immediate one` right while implementing the `rco-exp` and `rco-atom` compo. So my `rco-atom` didn't just replace each complex operation given to it with a new binding variable and an association list with the new binding and the evaluation of the expression with rco-exp.

But instead it tried to recurse on it with `rco-atom` until i hit a simple expression. That why i had to gather up all bindings, only use the latest new variable and process that "massive" list in `rco-exp` while building the new let forms and expression with the replaced variables.
It was way to complicated for what it was actually supposed to do and was also bggy in the way that sometimes a variable was used before it was later introduced by a let-binding.

After reading `Administrative normal form` (`http://ix.cs.uoregon.edu/~pdownen/publications/anf-continued.pdf`) and some other stuff on A-Normal Form i still didn't really get what i was doing wrong and why such a simple thing was so complex. Also reading about some CPS transformation stuff i was wondering why it isn't used in this educational compiler? From what i gasped it's pretty good at capturing the essense of the control flow and allow easier to implement optimizations. But will properly invest some more time in CPS stuff after this series is done.

Anyways after not looking or touching the code but only causually thinking about my solution for some days, basically let it sink in the easy way. I got the right idea after a dream about recursive patterns in s-expressions. So i first went back to the book chapter about it and read it carefully and instantly gasp the obvious. `rco-atom` should just return a new binding and call `rco-exp` for evaluating the expression for the binding. Also i rembered something like `not using structual recursion will complicate things quite a bit` from the video lecture, most likley the exact phrasing was different. 

Started a fresh and it took me about 2 hours to get it done including working through the racket documentation about multiple return `values` and the `for/lists` processing. If i had known how to use that stuff i would ended up with maybe 1 hour including testing. The new solution is also about half the size und much easier to understand what was going on.

I always find it fascinating and valuable how brutaly honest code and the computer can be. Even small misconception about something always lead to way to complicated and buggy code. But when thought about it again and again, will lead to much better understanding and much simpler code. Basically a good framework formalize computable ideas.

## Chapter 2.6 - Explicate Control
At first i thought that it would be a hard one. But it turned out to be much easier then the remove complex operants pass. Maybe partly because i got more found of thinking in structual recursion or/and that i am now a bit better at using racket. But more likely because of the excellent walkthrough from `Jeremie Siek` in the video lecture in the end of `https://iu.mediaspace.kaltura.com/media/Compiler+Course%2C+September+1%2C+2020/1_7o6702no`. There he explain the grammer for the pass but more importently, he actually also walk through an example recursion of the to functions `explicate-tail` and `explicate-assign`. 

Coded it up in about 2-3 hours. The hardest part wasn't actually the recrusive function. But instead figuring out that the interpreter/pass want the hole result returned as a `CFG` (guess stands for Control Flow Graph) that holds the `block*` that are basically a `symbol?` -> `Seq?` mapping based on an assoc list. And there is for now only one start block relevant: `start` -> `Seq`. 
Later chapters that handle functions, conditions and jumps should introduce more control flows blocks. Only gasp that after reading the hole `interp-C0.rkt` and parts of `utilities.rkt` and rewatching some part of the video lecture.

## Chapter 2.7 - Select Instructions
A good night session later that pass is also done. As with the passes before, i needed to find out what the actual output grammer of the pass is by reading part of the `interp.rkt` and `utilties.rkt` files. If the input and output grammer for each pass would be provided like in the nano pass framework it would been much easier to follow along. But regarding the learing expierence im not completely sold that it would be a better approach. While reading the inards of the interpreters to understand what is the actual output grammer is, i get a better understanding about the whole support files and how they work.

This time i would say that the book chapter was better then the video lekture about this pass but the tip about the debug level paid dividends later on. It defined the wanted output better and also made better suggestions for the implementation. At first i didn't get why 3 auxiliary functions `atm`, `stmt` and `tail` would be a good idea and tried to pack it into 2 `stmt` and `tail`. But in the end i ended up with exactly the those 3 function because it is easier to reuse the `atm` instead of reimplemend it in the `stmt` function again multiple times.

I also implemented the suggested optimizations for `+` and `-`. At first i was wondering why it still used the `moveq` statements but pretty fast recognized, with help of the mentioned activation of debugging information and the `abstract-syntax` option. That my `remove-complex-control` didn't reuse the introduced binding of the let-statment if it was in operator position. Instead instead it produced unnessary temp binding like:
```
(+ (let ([a 3]) (+ a 1)) 3) -> (let ([temp1 (let ([a 3]) (+ a 1))])
                                  (+ temp1 3))
```
So i fixed that by first changing `rco-atom` so that it returns an association list instead of just a (cons var binding) cell. So now the let binding will return 2 bindings. One for the variable and one for the body that reuse the varibale binding. We don't need it afterwards anyways. The example would now return something like that:
```
(values (Var a)
        (((Var a) . (Int 3))
         ((Var a) . (Prim '+ ((Var a) (Int 1))))))
```
And the rco-exp will compose that result with the other expression(s) to something like
```
(let ([a.1 3])
  (let ([a.1 (+ a 1)]) ; that a.1 is the same! it's more like a set! then a let ;)
    (+ a.1 3)))
```

After that the explicate-control pass translated the reused let binding vars to multiple `(Assign (Var x) (..))`. I also `removed-duplicates` from the introduced `explicate-control` program variables. Didn't found a function to add a unique value to a list in racket but works fine.

Overall this pass was the most rewarding so far by seeing the pseudo x86 assembler and makings some simple optimizations that reduced the move instructions. Curiously i even found the "assembler" more readable, then the nested structures of the abstract syntax tree from the passes before. Not sure exactly why but i may invest some time into the why in the future.

## Chapter 2.8 - Assign Homes

That pass was strait forward and the easiert so far and similar to the `uniquify` pass with the mapping of the `Var`s in the instructions to the stack locations. I have really not much to say about it, it just does this:
* Calculate the `stack-size` with `(* (round (/ (length locals) 8)) 16)` so it is a muliple of 16. 
* Create a mapping for each `local` -> `(Deref 'rbp stack-counter)`, the stack-counter begins by -8 and is decreased each time by 8. 
* Replace each Var in the instructions of the start block with the newly created mapping. 
* At the end just return the new Program with the info field extended with the stack-size and a new start Block with the mapped instructions. 
  * Had to modify `explicate-control` that it return an association list of `((locals . ...))`, so we can add the `stack-size` for later use in the print-x86 pass.

## Chapter 2.9 - Patch Instructions

Now that was even easier then the `assign-homes` pass. Map over each instruction an just match `(Instr name (list (Deref mem1 off1) (Deref mem2 off2)))` and replace it `(list (Instr 'movq (list (Deref mem1 off1) (Reg 'rax)))
(Instr name  (list (Reg 'rax) (Deref mem2 off2)))` then flatten the result and use it as the Block-instr*. And be done with it.
Later on there may be more instructions that need to be replaced but right now that should be sufficient. I verified with the line regex `Deref.*Deref` on output of the `(debug-level 1)` from `run-tests.rkt` run. Before the patch-instruction there where 17 matches, after the patch-instruction there are also 17 matches. So each one was replaced. Also checked 1 by hand. Seems fine so far.
Also added the given test and also one with `(let ([var (+ var var)]) ..)` and one with `(let ([var (read))] ..)`.

## Chapter 2.10 - Print x86
The implementation was pretty strait forward. I implemented the generation of the `main` and `conclusion` by first creating new blocks that i added to the `CFG` blocks. And then generate the x86 assembly strings with it. The `main` block also hold the `"    .globl main"` in it's info field as assoc list `((prelude . "    .globl main". For the label generation to also support MacOs, i made an extra function `print-x86-label` so it should also work for Macs but can not test it.The generation itself is just a map over the `CFG` assosc list, that generate the code for it, nothing fancy.

What really sucked was that i didn't just return integer in the range [0-255] in my tests. So i always got error for my tests that returned negativ integers. At first i thought that maybe `negq` only support a register and changed my compiler accordingly. But that didn't work, so i debugged the assembly with `gdb` and didn't found any errors. Only leared a bit about assembly debugging `x/4dg $sp` was pretty handy to get the 4 stack locations that where relevant. Also `info registers` or `i r` was pretty handy too. Anyways after i made sure i return the right result, i checked the internet about program return code, there it was! Only the lower 8 Bit are used for the program return code. And the negativ ones will be interpreted as `256 + result` because of the two-complement stuff. For example `-9` would turn to `256 + (-9) = 247`. So i just put my negative tests into another `(- original-test)` and was lucky that i don't have tests > 255 or < -255.

The course most likely use the program return code for symplictiy reasons but not sure if that's a good trade of. For example just printing the output and read it in again would be much more useable and wouldn't come with such a big constraints about what can be returned. But on the other side, that extra print function in C (?) must also be understood.

Another thing that bugged me was the creation of the `*.res` files. At first i tried to just use the interpreter on the original file and use that as result to compare to. But it didn't work for cases where there was input. After rereading the `check-passes-suite` and understanding how it does handle this case, with `with-input-from-file`, i just copied the code from it and placed it into the `compiler-tests-suite` and made it to a string for the compare function. Also needed to extended the `compiler-test-suite` and `compiler-tests` to take the initial-interpreter, in that case `interp-R1`. And voila it work wonderful and i also don't need to create any `*.res` files anymore. Much better and i don't see any downside to it. So not sure why the result files are even needed, maybe i later chapters it will hit me hard and there is actually a good reason for it.

## Chapter 2.10 - Partial Evaluator for R1

### Exercise 8
Was pretty easy, just add the new Forms `Var` `Let` to pe-exp and add the new `pe-R1` pass as first pass. Done.

### Exercise 9
Was a bit more involed but funny. At first i just switched each `(+ x (Int y))` to `(+ (Int y) x)` before passing it to the `pe-add` function. And in the `pe-add` i added a case `((Int x) (+ (Int y) e) -> (+ (Int (fx+ x y)) (pe-exp e))` That already take care of the given example in the book `(+ 1 (+ (read) 1))`.
Of course if both expressions are complex expressions that did't work, so i added some more cases to extract the first `(Int n)` of the subexpressions for all cases.
Then i tackled the let form. When we first `pe-exp` the let binding and the bodyand make some checks we can simpliy some cases:
* `(let ([x e] (Var x))` -> `e`
* `(let ([x e] (Var y))` -> `(Var y)`
* `(let ([x e] (Int n))` -> `(Int n)`
That handle almost all test cases but for example
```
(let ([x 32])
  (+ (let([x 10]) x) x))
```
Didn't get reduced to 42 but to
```
(let ([x 32])
  (+ 10 x))
```
But because we are working with a functional subset of Racket, we are able to inline every variable for it's value. To not expanding to more code i restricted that to `Int` only. And of course we must be careful about the bindings to i moved the pass after the `uniquify` pass. So we can savely replace `(Var x)` with is bounded to `(Int n)` with `(Int n)`. After adding the inlining and `pe-exp` on the inlined code the above example boild down to 42. Almost all passes are now just a number. Only passes that have a read funktion can't be reduced to a number.

My tests didn't cover it so i realized it only at the end. But `(- (+ 1 x))` should be reduced to `(+ -1 x)`.

I think there may still be some cases that i didn't cover but all my test programs except 2 boild now down to some assembler like:
```
    .globl main
main:
    pushq %rbp
    movq %rsp, %rbp
    subq $0, %rsp
    jmp start

start:
    movq $42, %rax
    jmp conclusion

conclusion:
    addq $0, %rsp
    popq %rbp
    retq 
```
Pretty neat. But for later passes Chapters i most likely will comment out this pass or testing will be a pain in the butt ;)

## Codereview - Chapter 2.4 - 2.10

Just looked the code review [lecture 5](https://iu.mediaspace.kaltura.com/media/Compiler+Course/1_vizyqbn0) and changed some code or add some comments here and/or in the code about the why i choose another way to implement the pass.

### Chapter 2.4 - Unifify
I used the `let*` for binding the temp variables and actually found it also more readable. Because the body of the `let*` is ident a bit to the right and make it easier to recognize the statement that will be executed last. But maybe it's just me. And also the tip that the `define` is shorter the `let`/`let*` if it's nested multiple time. 
Also had the bug with using the `new-symtab` with the new binding on the binding expression at first, but recognized the bug by testing around in the repl with: 
```
(let ([a 1])
  (let ([a a])
    (+ a (let ([a 4])
           a))))
```
The interp-R1 screamed `undefined 'a2' before useage` or something like that. Because it would expand to something like:
```
(let ([a1 1])
  (let ([a2 a2])
    (+ a2 (let ([a3 4])
           a3))))
```
Added that as test `r1_07.rkt`.

I also used the generic `dict-ref` for the lookup and extending the enviroment.

### Chapter 2.5 - Remove Complex Operators
In the `rco-atom` i am just using `rco-exp` in `Let` and `Prim` cases but with a twist. The `Let` will reuse the x binding for the body and use the back-quote for a more declarative approach. So it avoids introducing new unessary let variables bindings. I also use the optional parameter `[var (gensym 'temp)]` to generate the new binding for `Prim` expressions. Did orginaly plan to reuse some more bindings but it was to complex. 
But after seeing the `rco-exp` solution i adapted `rco-exp` to also use the `make-let` function to simplify it, but names it `warp-into-lets` to make it more clear what it does. Before I used a `foldr` with multiple `append`, `remove-duplicates` and `remove*` to get rid of the empty bindings in the simple `Var` and `Int` cases of `rco-atom` and to create the `let`-bindings. Could also remove the extra layer of lists in `rco-atom` because of the use of `assoc*` for the bindings in `rco-exp`. After that i found my code more readable then before and also a bit better then Jeremy Sieks solution. But judge for yourself:
* Jeremy Sieks solution:
```
(define (rco-atom e)
  (match e
    [(Var x) (values (Var x) '())]
    [(Int n) (values (Int n) '())]
    [(Let x rhs body)
     (define new-rhs (rco-exp rhs))
     (define-values (new-body body-ss) (rco-atom body))
     (values new-body (append `((,x . ,new-rhs)) body-ss))]
    [(Prim op es)
     (define-values (new-es sss)
       (for/lists (l1 l2) ([e es]) (rco-atom e)))
     (define ss (append* sss)) ; joins the bindings and remove empty ones
     (define tmp (gensym 'tmp))
     (values (Var tmp)
             (append ss `((,tmp . ,(Prim of new-es)))))]))

(define (make-lets bs e)
  (match bs
    [`() e]
    [`((,x . ,e^) . ,bs^)
     (Let x e^ (make-lets bs^ e))]))

(define (rco-exp e)
  (match e
    [(Var x) (Var x)]
    [(Int n) (Int n)]
    [(Let x rhs body)
     (Let x (rco-exp rhs) (rco-exp body))]
    [(Prim op es)
     (define-values (new-es sss)
       (for/lists (l1 l2) ([e es]) (rco-atom e)))
     (make-lets (append* sss) (Prim op new-es))]))
```
* My new solution:
```
(define (warp-into-lets bs e)
  (match bs
    [`() e]
    [`((,x . ,e^) . ,bs^)
     (Let x e^ (warp-into-lets bs^ e))]))

(define (rco-exp exp)
  (match exp
    [(Var x) (Var x)]
    [(Int n) (Int n)]
    [(Let x e body) (Let x (rco-exp e) (rco-exp body))]
    [(Prim op es)
     (define-values (new-es bindings)
       (for/lists (new-es bindings) ([e es]) (rco-atom e)))
     (warp-into-lets (append* bindings) (Prim op new-es))]))

(define (rco-atom exp [var (gensym 'temp)])
  (match exp
    [(Var x) (values (Var x) '())]
    [(Int n) (values (Int n) '())]
    [(Let x e body) (values (Var x) `((,x . ,(rco-exp e)) ;; rebind the let (Var x) outside the (Prim op args*)
                                      (,x . ,(rco-exp body))))] ;; and reuse the (Var x) also for the result of the body
    [(Prim op es) (values (Var var) `((,var . ,(rco-exp (Prim op es)))))]))
```

### Chapter 2.6 - Explicate Control
Pretty much the same code as in code review. Also adapted the `[else (...)]` for the redundant cases in `explicate-tail` and `explicate-assign`. The only difference is that my `explicate-assign` also returns a variable in the else case. Think that was overseen in the code review, otherwise the info field in the program will always be empty.

### Chapter 2.7 - Select Instruction
I had extra functions for the cases when the `(select-assign var exp)` `var` was part of the `+` or `-` primitives. Adapted the `#:when (equal? x var)` guards on the matching and moved the instructions generation into `select-assign`. Also now use the `[else (...)]` in `select-assign` to generate the simple `movq` instructions for `(Var x)` and `(Int n)`.
Otherwise it's pretty much the same only don't have a special case for the `[(Return (Prim 'read (list))) ...]` in `select-tail` so that i could remove a `movq`. Think that complicates the code to much that it's worth it. My `patch-instructions` should remove that later on.

### Chapter 2.8 - Assign Homes
Picked the good points from the code review:
* own function for block handling and also iterate over all labeled blocks of `CFG`
* using `index-of` function for calculating the variable positions
* The now called `assign-home-exp` matches `(Var x)` and replaces it with the `Deref` from the mapping. The instruction args are now just mapped to `assign-home-exp` instead of new lambda
* replaced the `map` useages with `for/list`, it's a bit shorter
Overall the code for `assign-homes` is now a bit shorter an more readable. In general the useage of pattern matching and extra functions for different stuff make it more compact and readable in my opinion.

### Chapter 2.9 - Patch Instructions
Had just a let in the `patch-instructions` that would `flatten` and `map` over the instructions and generate new ones and return the new `Program`. Refactored it to now also having dedicated functions `patch-block` and `patch-instr` for blocks and instructions. Also iterate now over all blocks in `CFG`.
It made the hole pass a bit bigger but found it more readable and consitend with the rest. That's much more importend than the line count.

### Chapter 2.10 - Print x86
Compared to the shown solution i opted to generate the `conclusion` and `main` blocks and then map over the blocks and generate the string output. The `    .globl main` is handled by putting it into assoc-list with `prelude` as key.
I also adapted the given `utilities.rkt` functions `align` in the `assign-homes` pass and the `label-name` in the `print-x86` pass. Instead of writting it myself. 

### Conclusion of the Code Review
Found the code review a nice expierence. Now i know a bit more about how to use Racket and also about factoring the code a bit better into functions, mostly based on the grammer for clarity, and the provided pattern matching from racket. Didn't found any bugs to correct in my code from the code review but some explanations where nice to reinforce what i have already learned and restructure my codebase a bit. At some places i would have liked to ask questions and have a bit of a discussion about the pros and cons why doing things that specific way. But that can't be changed! Overall i find `Jeremey Siek` did a fine job on it :)

Learned also a bit `x86`-Assembler what is never a bad thing to have under ones belt. How to debug assembly code with `gdb` may also come in handy to in the future!

The only downside is that i am a bit disappointed by Racket itself for not having more generic functions, so for many cases you need to find/lookup the specific functions to use for the data structure. Instead of just remembering 1 function for some concept that would work on all provided data structers if it make sense.
For implementing the passes it wasn't so bad, because we dealt mostly with predefined structures and list. Would also be nice if the pattern matching was a bit more capeable. For example matching `(... (Var x) (Var x) ...)` only when `x` is in both `Var`s would be a fine thing. Instead it must be handeled with guards like `(.. (Var a) (Var b) ..) #:when (equal? a b)`. Also using can't bind outside variables and use them in the match directly, only over guards. At least as far as i know, didn't really look to depth into the `match` functionality but in principile is see much potential in general pattern matching. First class pattern matching, anyone?

## Chapter 3.2 - Liveness Analysis
Generating the live variable sets was strait forward. Just followed the recommendation in the book for the implementation:
* `live-after-instr` calculate the set of live variables for an instruction based on the last live set, both given
* `write-vars-set-instr` for getting the variables that are written to by the given instruction
* `read-var-set-instr` for getting the variables that are read by the given instruction
* `live-after` just iterate over the reversed instruction list and and empty live set and accumulates the new live set based on `live-after-instr` until no instructions are left
The rest is just boilderplate code for extracting the block, exented the info field etc.

The only bumpstone was that i didn't handle the `movq` right in the `read-var-set-instr` at first. So also the target of the write did get written with the result that just all `Var`s ended up in the live set ;)

The one think i didn't do, is to return the vars for the caller saved registers in my `write-vars-set-instr`. I don't touch the registers for example `rax` until i understand how they should be reallocated without breaking the code. At first i had the `(Var 'rax)` in the instructions. What isn't quite correct but the later `patch-instructions` pass did convert `(Var 'rax)` -> `(Reg 'rax)`.

Also the example in the book don't list the registers for the following example:
```
(let ([v 1])
  (let ([w 42])
    (let ([x (+ v 7)])
      (let ([y x])
        (let ([z (+ x w)])
          (+ z (- y)))))))
```
My live set variables and the instructions now match exactly the example in the book:
```
(Block
 (list
  (list
   'live-after
   (set)
   (set 'v169789)
   (set 'v169789 'w169790)
   (set 'x169791 'w169790)
   (set 'x169791 'w169790)
   (set 'x169791 'y169792 'w169790)
   (set 'z169793 'y169792 'w169790)
   (set 'z169793 'y169792)
   (set 'z169793 'temp169862)
   (set 'z169793 'temp169862)
   (set 'temp169862)
   (set)
   (set)))
 (list
  (Instr 'movq (list (Imm 1) (Var 'v169789)))
  (Instr 'movq (list (Imm 42) (Var 'w169790)))
  (Instr 'movq (list (Var 'v169789) (Var 'x169791)))
  (Instr 'addq (list (Imm 7) (Var 'x169791)))
  (Instr 'movq (list (Var 'x169791) (Var 'y169792)))
  (Instr 'movq (list (Var 'x169791) (Var 'z169793)))
  (Instr 'addq (list (Var 'w169790) (Var 'z169793)))
  (Instr 'movq (list (Var 'y169792) (Var 'temp169862)))
  (Instr 'negq (list (Var 'temp169862)))
  (Instr 'movq (list (Var 'z169793) (Reg 'rax)))
  (Instr 'addq (list (Var 'temp169862) (Reg 'rax)))
  (Jmp 'conclusion)))
```
But im still unsure about how the register reallocation thing should work if it encounter a register. Anyways i also added the 2 examples from the book to my tests and it looks fine so far.

## Chapter 3.3 - Building the Interference Graph
In principle it was easy but working with the graph lib when it comes to repl and print support for debugging is not nice. Otherwise my implementation is just the boilderplate code to extract the block and adding the new generated graph based on the rules in the book to the 'conflict block info field.

Debugged the hole thing by using the previous pass debug output and feed the block to `build-interference-block` that for debugging purposes only return the graph. Used `(display (graphviz (build-interference-block ...)))` to generate the following graph by using example `tests/r1_18.rkt`. With is the same as in the book:

![r1_18.rkt interference graph](images/r1_18-interference-graph.png "r1_18 Interference Graph")

Also worked with `displayln` and `format` to work out some false variable useage bugs. And compared the `Figure 3.4: Interference results for the running example` step by step. Looks fine so far. But still not consider the registers and variables only.

## Chapter 3.4 & 3.5 - Allocate Registers / Print x86

My first implementation just created a colored graph without considering the used callee registers. Of course that went badly as soon a program called a function aka. `(read)`. I struggled a bit to understand the calling convention, don't ask me why, but after reading a bit on the internet about it i finally got what the `caller-saved` and `callee-saved` registers are. Found the wikipedia description with `volatile` and `non-volatile` did a much better job at that then the book. In retro perspective i wouldn't say the book don't explain it good enought more that i needed a differend rehash to really grasp the idea behind it.

After i got that under my belt i was clear to me that the my `color-graph` function should consider that live variables that are used at a call statement should never use the `caller-saved` registers. But wasn't really sure how to do just that, so i reread all Chapters 3.1-3.5. It still didn't click, even if that stands there black on white, that the interference graph should add for each live variable to all `caller-saved` registers. That then could be used later on. Of course i first tried a different way of just marking the locals with a new info property `used-at-call` that should be used by the `color-graph` but i messed it by making it way to complicated and never finished that path.

Instead i cheated a bit by peeking into the codereview only looking the `allocate-register` pass. It then hit me hard what was meant by adding the `caller-saved` registers in the interfence graph for all variables that are live at a call. And did go that way. Went pretty smoothly then but still dislike the useages of the registers as if they where normal variables and the then needed `locals` to only handle the relevant variable vertices. Maybe i will restructe it later on.

Anyways to get the code going i needed to also modify the `live-after`, `build-interfence` and `print-x86` pass. The interference graph `conflicts` works now over all blocks of a `CFG` and should add the `caller-saved` registers for live variables at a call statement. Also changed `live-after` to only consider real variables with may be a failure that will bite me later. But the `color-graph` also ignores them so they are of no real use. The `print-x86` now gets the num of spilled vars and also all `callee-saved` registers so it can save them in the function `prolog` and restore them later on.

At that point i had it working, but ironically even fewer tests where successful. The problem was that i calculated the stack size false. Because i missed the imported point that `pushq` already decreases the stackpointer by `8`. Changed the calculation that it just align the spilled vars to `16` and add `8` if the `used-callee` is uneven. That may waste `16` bytes in the worstcase if there are uneven spilled vars and used callee registers. But fixed that now by adding the unaligned callee to the arguments first then align them to 16 byte boundary and then subtract the 8 byte pushed  unaligned callee. Then every thing is aligned and no space is wasted.

Also because i reuse the let binding variable also for the body, because it can't be used outside of the body. I get away with less assignment statements and in turn also less move statments. So i already have eleminated the move from the move biasing example:
```
    .globl main
main:
    pushq %rbp
    movq %rsp, %rbp
    subq $0, %rsp
    jmp start

start:
    movq $1, %rcx
    movq $42, %rsi
    addq $7, %rcx   
    movq %rcx, %rdx # could already be moved to %rax to save the move below
    addq %rsi, %rdx
    negq %rcx
    movq %rdx, %rax # unessary move if that would be already in %rax
    addq %rcx, %rax
    jmp conclusion

conclusion:
    addq $0, %rsp
    popq %rbp
    retq 
```
But there is still room for improvment with the move basing if `movq %rcx, %rdx` would already be a `movq %rcx, %rax`. But but it must make sure that `%rax` isn't written in between. Most likely the Move Biasing Challenge won't go into that but it would be nice, let's see what it actually brings.

Overall a really good Chapter so far, learned a lot of stuff. Like graph coloring with the DSatur alogrithmen, what is an interference graph and how to construct them from a live analyses. And of course understanding the x84 (amd64) function calling convention with the `caller-saved` and `callee-saved` registers.

### Chapter 3.6 - Challenge move Biasing
Was also pretty strait forward except for some reason my useage of the racket `heap` did get broken in the progress. I tried for a bit to find the cause but after some fruitless debugging i just replaced it with a list of the symbols that i just sort with the new conditions. Also moved the `conflicts` and the new `moves` graphs into the program info, later on we will have more then 1 block so i can't place them into the block info field.

The moves for the `r1_18` tests tesult into the same graph as in the book that looks something like:
![r1_18.rkt move graph](images/r1_18-move-graph.png "r1_18 Move Graph")

After extensive debugging because of the broken `heap` i am pretty sure the implemention is correct but of course it didn't find the unessary move to `%rdx`. Just for fun i refeed the pseudo-x86 with the registers as `Var`s into the 3 passes `uncover-live`, `build-interference` and `allocate-registers` again with the registers as the locals. And looked at the assigned colors in the `allocate-register` pass and voila `rax` had the same color as `rdx` in the `r1_18.rkt` test. But didn't test that against some other tests where `rax` would get overridden in between. Most likely i would not work because the variables are the same and would not interfere with itself and just get overwritten. Didn't have the muse to follow that path because of the extra code needed and the unclear benefits and even slower compiler in the end. So not sure it would be worth the time.

### Code Review Chapter 3

Overall i leared the most so far from that chapter and also found it the most interresing topic to far. It was always a mystery for me how to map the much higher variables numbers to a limited number of registers and what's the actual calling convertion `AMD64` is all about. Only had done a bit of `x86` with spilling all variabels to the stack schema.

Also that's was the first time i really used some graph alorithmen like the `DSatur` graph coloring with move biasing. Quite interresting! Most likely will look a bit deeper into that and optimizations in general after i am done with the hole course/book.

I'm pretty eager to inhale the next chapters, in particular the `Garbage Collection` (Chapter 5), how to handle `Lexically Scoped Functions` aka. `Closures` (Chapter 7). And `Dynamic Typing` (Chapter 8). Saddly the later chapters about `Gradual Typing`, `Parametic Polymorphism` and `High-level Optimizations` are empty except for some references. But will look into them and see what i can get done, maybe i will also contact `Jeremy Siek` about it if i'm stuck at it.

Don't regret a second so far from that excellect course, was a nice ride so far and hope that course will be keeped alive, improved and get expanded. Keep up the good work and enthusiasm about compilers `Jeremy Siek`! Thank you so it :)

#### Print x86
I made a bit of the different `prolog`/`conflusion` in the way that may frame starts after the spilled `callee-saved` registers. For example my prolog looks like that. That simplified the x86 generation and calculation of offset for the spilled variables:
```
    pushq %rbp
    pushq %rbx
    movq %rsp, %rbp # function frame starts after the the calle-saved registers are pushed to the stack
    subq $8, %rsp
    jmp start
```
Instead of the normally used calling convention that would look like that:
```
    pushq %rbp
    movq %rsp, %rbp # standard calling convention where the frame starts after the return address and the old pushed %rbp  
    pushq %rbx
    subq $8, %rsp
    jmp start
```
But because my frame starts after the spilled variables i can just count downwards from `-8` instead of `-16` for the spilled variables. An alternative that would also spare me to take the callee-saved registers into account. Is by first make place for the spilled variables and then push the callee-saved registers. For that example it would like that:
```
    pushq %rbp
    movq %rsp, %rbp
    subq $8, %rsp    # first make place for the spilled variables
    pushq %rbx       # and then push the callee-saved registers
    jmp start

```
Adjusted the last version. It's not much more complicated but most debugging tools like `gdb` should handle that better then my first one. Thanks for the question from a student, they did most likely the same. The only thing that may be a problem is if we go into the `red-zone` from the `SYSTEM V AMD64 ABI` after 256 offset to the base frame pointer. Then my first version would even be a bit on the saver side because it pushed the callee saved registers before the frame pointer. But i don't handle that case anyways and not sure what's that all about only read a paragraph on wikipedia about it.


#### Uncover live
Apart from some minor differences it's the same. The things that are different appart from the structure is:
* I don't handle `Reg`s but only `Var`s because up to that point my compiler only works on `Var`s. The `allocate-register` pass will introduce the Registers. But maybe i will change that in the future to i can make multiple passes and can get rid of some more moves or register spills
* Think also the handling of the case `[(Deref r i) (set r)]` in `free-vars` from him is also wrong. Because if it would be used, not that register `%r` would be live but `i(%r)`. So the liveness analyses would be incorrect. But not 100% sure about it.

#### Build Interference
Aside from some structal differences and that i also generate the move graph it's basically the same. So nothing to to speak of really.

#### Register Allocation
Actually still had a small bug in the move biased color selection. If we ran out of registers in the move biased and the unbiased color selection. We want to choose the move biased one, because we spill anyways and may use less spilled and/or moved vars that way.

Used the part of the code from the review for the move biased color selection. Aka the `for/first` that `Jeremy Siek` used that returns `#f` if it don't find anything that can be used quite nicely on test case for the color selection.

